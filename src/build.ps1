
# Update to latest version. Could be ommitted if fixed version desired.
dotnet tool update BBC.Studios.Common.BuildScript 

# Aquire the common build script modules using nuget/dotnet tool
dotnet tool restore
$module = dotnet common-buildscript get-module-folder
Import-Module -Name $module\build-functions.psm1 -Force

# Set some build params
$config = "Release"
$project = "Web"
$packageId = "ApiExample.Web"
$publishedFolder = "$ReleaseDir\$project"

# Execute the build steps - Clean > Restore > Compile > Test > Pack > Results
Install-Octo
Invoke-CleanCore -Config $config
Restore -UseDotNet
Invoke-BuildCore -Config $config
Invoke-TestCore -ProjectNameFilter "*Test*" -Config $config

# Publish web or api project, zip it up and remove the unzipped output
# The send package to Teamcity and Octopus
Invoke-PublishCore -Project $project -Config $config
$appPackage = New-ZipPackage -id "$packageId.Web" -version $BuildNumber -FolderToZip $publishedFolder -outputFolder $ReleaseDir -RemoveFolder

Publish-TCArtifacts $ReleaseDir
Publish-ToOctopusGallery $appPackage
ReportResults
