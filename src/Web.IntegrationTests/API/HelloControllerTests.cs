using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using NUnit.Framework;

namespace ApiExample.Web.IntegrationTests.API
{
    [TestFixture]
    public class HelloControllerTests
    {
        
        private readonly string HelloApiUri = "api/Greetings/Test User";
        
        private WebApplicationFactory<Startup> applicationFactory;
        private HttpClient httpClient;

        [OneTimeSetUp]
        public void SetUp()
        {
            applicationFactory = new WebApplicationFactory<Startup>();
            httpClient = applicationFactory.CreateClient();
        }

        [Test]
        public async Task Get_ReturnsCorrectContentType()
        {
            var result = await httpClient.GetAsync(HelloApiUri);

            Assert.AreEqual("application/json; charset=utf-8",
                result.Content.Headers.ContentType?.ToString());
        }

        [Test]
        public async Task Get_ReturnsGreetingMessage()
        {
            var httpResponse = await httpClient.GetAsync(HelloApiUri);
            var json = await httpResponse.Content.ReadAsStringAsync();
            dynamic responseContent = JsonConvert.DeserializeObject(json);

            string message = responseContent.message;
            Assert.That(message, Is.EqualTo("Welcome to BBC Studios Test User"));
        }

        [Test]
        public async Task Get_ReturnsSuccessStatusCode()
        {
            var result = await httpClient.GetAsync(HelloApiUri);

            result.EnsureSuccessStatusCode();
        }
    }
}