﻿using ApiExample.ApplicationCore.Services;

namespace ApiExample.ApplicationCore.Greetings
{
    public class GreetingService : IGreetingService
    {
        public string GetWelcomeMessage(string name)
        {
            return "Welcome to BBC Studios "+ name +"";
        }
    }
}