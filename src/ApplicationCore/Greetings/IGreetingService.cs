﻿namespace ApiExample.ApplicationCore.Services
{
    public interface IGreetingService
    {
        string GetWelcomeMessage(string name);
    }
}