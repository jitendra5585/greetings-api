using ApiExample.ApplicationCore.Greetings;
using NUnit.Framework;

namespace ApiExample.ApplicationCore.UnitTests.Greetings
{
    public class GreetingServiceTests
    {
        private GreetingService sut;

        [SetUp]
        public void Setup()
        {
            sut = new GreetingService();
        }

        [Test]
        public void GetMessage_ShouldReturnHelloWorldMessage()
        {
            var expectedMessage = "Welcome to BBC Studios Test User";

            var result = sut.GetWelcomeMessage("Test User");

            Assert.AreEqual(expectedMessage, result);
        }
    }
}