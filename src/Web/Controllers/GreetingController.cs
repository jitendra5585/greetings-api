﻿using ApiExample.ApplicationCore.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiExample.Web.Controllers
{
    [ApiController]
    [Route("api/Greetings")]
    public class GreetingController : ControllerBase
    {
        private readonly IGreetingService greetingService;
        private readonly ILogger<GreetingController> logger;

        public GreetingController(IGreetingService greetingService, ILogger<GreetingController> logger)
        {
            this.greetingService = greetingService;
            this.logger = logger;
        }

        [Route("{name}")]
        [HttpGet]
        public ActionResult<string> Get(string name)
        {
            logger.LogInformation("Getting greeting message for " + name + ".");

            var greetingMessage = greetingService.GetWelcomeMessage(name);

            return Ok(new { Message = greetingMessage });
        }
    }
}